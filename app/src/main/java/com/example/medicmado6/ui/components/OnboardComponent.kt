package com.example.medicmado6.ui.components

import androidx.compose.foundation.layout.*
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.medicmado6.ui.theme.descriptionColor
import com.example.medicmado6.ui.theme.lato
import com.example.medicmado6.ui.theme.sourceSans
import com.example.medicmado6.ui.theme.titleColor

/*
Описание: Компонент приветственного экрана
Дата создания: 30.03.2023
Автор: Георгий Хасанов
 */
@Composable
fun OnboardComponent(
    title: String,
    text: String
) {
    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier
            .widthIn(max = 220.dp)
            .fillMaxWidth()
    ) {
        Text(
            title,
            fontSize = 20.sp,
            fontWeight = FontWeight.SemiBold,
            fontFamily = lato,
            color = titleColor,
            textAlign = TextAlign.Center
        )
        Spacer(modifier = Modifier.height(29.dp))
        Text(
            text,
            fontSize = 14.sp,
            fontWeight = FontWeight.Normal,
            fontFamily = sourceSans,
            color = descriptionColor,
            textAlign = TextAlign.Center
        )
    }
}