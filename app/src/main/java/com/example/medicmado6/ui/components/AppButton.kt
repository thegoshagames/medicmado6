package com.example.medicmado6.ui.components

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.padding
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.medicmado6.ui.theme.*
import com.example.medicmado6.R
/*
Описание: Стандартная кнопка приложения
Дата создания: 30.03.2023
Автор: Георгий Хасанов
 */
@Composable
fun AppButton(
    modifier: Modifier = Modifier,
    enabled: Boolean = true,
    text: String,
    fontSize: TextUnit = 17.sp,
    fontWeight: FontWeight = FontWeight.SemiBold,
    fontFamily: FontFamily = sourceSans,
    color: Color = Color.White,
    borderStroke: BorderStroke = BorderStroke(0.dp, strokeColor),
    colors: ButtonColors = ButtonDefaults.buttonColors(backgroundColor = primaryColor, disabledBackgroundColor = primaryDisabledColor),
    contentPadding: PaddingValues = PaddingValues(16.dp),
    onClick: () -> Unit
) {
    Button(
        modifier = modifier,
        enabled = enabled,
        contentPadding = contentPadding,
        elevation = ButtonDefaults.elevation(0.dp),
        colors = colors,
        border = borderStroke,
        onClick = onClick
    ) {
        Text(
            text = text,
            fontSize = fontSize,
            fontWeight = fontWeight,
            fontFamily = fontFamily,
            color = color
        )
    }
}

/*
Описание: Стандартная кнопка приложения
Дата создания: 30.03.2023
Автор: Георгий Хасанов
 */
@Composable
fun AppBackButton(
    modifier: Modifier = Modifier,
    onClick: () -> Unit
) {
    Card(
        backgroundColor = inputColor,
        elevation = 0.dp,
        shape = MaterialTheme.shapes.small,
        modifier = modifier.clickable {
            onClick()
        }
    ) {
        Icon(
            painter = painterResource(id = R.drawable.ic_back),
            contentDescription = "",
            tint = textColor,
            modifier = Modifier.padding(6.dp)
        )
    }
}