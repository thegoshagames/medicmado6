package com.example.medicmado6

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.window.Dialog
import androidx.lifecycle.ViewModelProvider
import com.example.medicmado6.ui.components.AppBackButton
import com.example.medicmado6.ui.components.AppTextField
import com.example.medicmado6.ui.theme.MedicMADO6Theme
import com.example.medicmado6.ui.theme.descriptionColor
import com.example.medicmado6.ui.theme.sourceSans
import com.example.medicmado6.view.PasswordActivity
import com.example.medicmado6.viewmodel.LoginViewModel
import kotlinx.coroutines.delay

/*
Описание: Класс экрана проверки кода из почты
Дата создания: 30.03.2023
Автор: Георгий Хасанов
 */
class CodeActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            MedicMADO6Theme {
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    ScreenContent()
                }
            }
        }
    }

    /*
    Описание: Контент экрана проверки кода из почты
    Дата создания: 30.03.2023
    Автор: Георгий Хасанов
     */
    @Composable
    fun ScreenContent() {
        val mContext = LocalContext.current
        val shared = this.getSharedPreferences("shared", Context.MODE_PRIVATE)

        val viewModel = ViewModelProvider(this)[LoginViewModel::class.java]

        var emailText by rememberSaveable { mutableStateOf(intent.getStringExtra("email").toString()) }

        var code1 by rememberSaveable { mutableStateOf("") }
        var code2 by rememberSaveable { mutableStateOf("") }
        var code3 by rememberSaveable { mutableStateOf("") }
        var code4 by rememberSaveable { mutableStateOf("") }

        var isLoading by rememberSaveable { mutableStateOf(false) }
        var isAlertVisible by rememberSaveable { mutableStateOf(false) }

        val errorMessage by viewModel.errorMessage.observeAsState()
        LaunchedEffect(errorMessage) {
            isLoading = false

            if (errorMessage != null) {
                isAlertVisible = true
            }
        }

        val responseCode by viewModel.responseCode.observeAsState()
        LaunchedEffect(responseCode) {
            isLoading = false
        }

        val token by viewModel.token.observeAsState()
        LaunchedEffect(token) {
            isLoading = false

            if (token != null) {
                with(shared.edit()) {
                    putString("token", token)
                    apply()
                }

                val intent = Intent(mContext, PasswordActivity::class.java)
                startActivity(intent)
            }
        }

        var timer by rememberSaveable { mutableStateOf(60) }
        LaunchedEffect(timer) {
            delay(1000)

            if (timer > 0) {
                timer -= 1
            } else {
                isLoading = true
                timer = 60

                viewModel.sendCode(emailText)
            }
        }

        Scaffold(
            topBar = {
                AppBackButton(
                    modifier = Modifier.padding(start = 20.dp, top = 24.dp)
                ) {
                    onBackPressed()
                }
            }
        ) { padding ->
            Box(modifier = Modifier
                .padding(padding)
                .fillMaxSize()
            ) {
                Column(
                    horizontalAlignment = Alignment.CenterHorizontally,
                    verticalArrangement = Arrangement.Center,
                    modifier = Modifier
                        .widthIn(max = 350.dp)
                        .fillMaxWidth()
                        .padding(horizontal = 20.dp)
                        .align(Alignment.Center)
                ) {
                    Text(
                        "Введите код из E-mail",
                        fontSize = 17.sp,
                        fontWeight = FontWeight.SemiBold
                    )
                    Spacer(modifier = Modifier.height(24.dp))
                    Row(
                        horizontalArrangement = Arrangement.Center,
                        verticalAlignment = Alignment.CenterVertically
                    ) {
                        AppTextField(
                            value = code1,
                            onValueChange = {
                                if (it.length <= 1) {
                                    code1 = it
                                }
                            },
                            modifier = Modifier.size(48.dp),
                            contentPadding = PaddingValues(0.dp),
                            textStyle = TextStyle(lineHeight = 0.sp, textAlign = TextAlign.Center, fontSize = 20.sp, fontFamily = sourceSans)
                        )
                        Spacer(modifier = Modifier.width(16.dp))
                        AppTextField(
                            value = code2,
                            onValueChange = {
                                if (it.length <= 1) {
                                    code2 = it
                                }
                            },
                            modifier = Modifier.size(48.dp),
                            contentPadding = PaddingValues(0.dp),
                            textStyle = TextStyle(lineHeight = 0.sp, textAlign = TextAlign.Center, fontSize = 20.sp, fontFamily = sourceSans)
                        )
                        Spacer(modifier = Modifier.width(16.dp))
                        AppTextField(
                            value = code3,
                            onValueChange = {
                                if (it.length <= 1) {
                                    code3 = it
                                }
                            },
                            modifier = Modifier.size(48.dp),
                            contentPadding = PaddingValues(0.dp),
                            textStyle = TextStyle(lineHeight = 0.sp, textAlign = TextAlign.Center, fontSize = 20.sp, fontFamily = sourceSans)
                        )
                        Spacer(modifier = Modifier.width(16.dp))
                        AppTextField(
                            value = code4,
                            onValueChange = {
                                if (it.length <= 1) {
                                    code4 = it
                                }

                                if (it.length == 1) {
                                    isLoading = true
                                    viewModel.checkCode(emailText, "$code1$code2$code3$code4")
                                }
                            },
                            modifier = Modifier.size(48.dp),
                            contentPadding = PaddingValues(0.dp),
                            textStyle = TextStyle(lineHeight = 0.sp, textAlign = TextAlign.Center, fontSize = 20.sp, fontFamily = sourceSans)
                        )
                        Spacer(modifier = Modifier.width(16.dp))
                    }
                    Spacer(modifier = Modifier.height(16.dp))
                    Text(
                        text = "Отправить код повторно можно будет через $timer секунд",
                        fontSize = 15.sp,
                        fontFamily = sourceSans,
                        textAlign = TextAlign.Center,
                        color = descriptionColor,
                        modifier = Modifier
                            .widthIn(max = 250.dp)
                            .fillMaxWidth()
                    )
                }
            }
        }

        if (isLoading) {
            Dialog(onDismissRequest = {}) {
                CircularProgressIndicator()
            }
        }

        if (isAlertVisible) {
            AlertDialog(
                onDismissRequest = { isAlertVisible = false },
                title = { Text(text = "Ошибка") },
                text = { Text(viewModel.errorMessage.value.toString()) },
                buttons = {
                    TextButton(onClick = { isAlertVisible = false }) {
                        Text("OK")
                    }
                }
            )
        }
    }
}